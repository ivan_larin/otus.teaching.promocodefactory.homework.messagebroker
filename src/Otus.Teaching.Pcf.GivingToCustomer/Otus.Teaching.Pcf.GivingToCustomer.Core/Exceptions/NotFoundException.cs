﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Exceptions
{
    public class NotFoundException: Exception
    {
    }
}
