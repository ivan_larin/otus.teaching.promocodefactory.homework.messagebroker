﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public class PromocodeService : IPromocodeService
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromocodeService(
            IRepository<PromoCode> promoCodesRepository,
            IRepository<Preference> preferencesRepository, 
            IRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }

        public async Task GiveAsync(string serviceInfo, Guid partnerId, Guid promoCodeId, string promoCode, Guid preferenceId, string beginDate, string endDate)
        {
            var preference = await _preferencesRepository.GetByIdAsync(preferenceId);

            if (preference == null)
            {
                throw NotFoundException();
            }

            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            var code = new PromoCode();
            code.Id = promoCodeId;

            code.PartnerId = partnerId;
            code.Code = promoCode;
            code.ServiceInfo = serviceInfo;

            code.BeginDate = DateTime.Parse(beginDate);
            code.EndDate = DateTime.Parse(endDate);

            code.Preference = preference;
            code.PreferenceId = preference.Id;

            code.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                code.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = code.Id,
                    PromoCode = code
                });
            };

            await _promoCodesRepository.AddAsync(code);
        }

        private Exception NotFoundException()
        {
            throw new NotImplementedException();
        }
    }
}
