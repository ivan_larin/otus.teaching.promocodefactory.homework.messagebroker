﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public interface IPromocodeService
    {
        Task GiveAsync(
            string serviceInfo, 
            Guid partnerId, 
            Guid promoCodeId, 
            string promoCode, 
            Guid preferenceId, 
            string beginDate, 
            string endDate);
    }
}
