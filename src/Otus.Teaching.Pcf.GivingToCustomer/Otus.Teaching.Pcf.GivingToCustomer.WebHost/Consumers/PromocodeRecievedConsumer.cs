﻿using MassTransit;
using Messages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class PromocodeRecievedConsumer : IConsumer<PromocodeRecieved>
    {
        private readonly IPromocodeService _promocodeService;

        public PromocodeRecievedConsumer(IPromocodeService promocodeService)
        {
            _promocodeService = promocodeService;
        }

        public async Task Consume(ConsumeContext<PromocodeRecieved> context)
        {
            await _promocodeService.GiveAsync(
                    context.Message.ServiceInfo,
                    context.Message.PartnerId,
                    context.Message.PromoCodeId,
                    context.Message.PromoCode,
                    context.Message.PreferenceId,
                    context.Message.BeginDate,
                    context.Message.EndDate);
        }
    }
}
