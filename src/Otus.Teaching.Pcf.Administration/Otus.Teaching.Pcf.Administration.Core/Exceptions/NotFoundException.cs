﻿using System;

namespace Otus.Teaching.Pcf.Administration.Core.Exceptions
{
    public class NotFoundException: Exception
    {
    }
}
