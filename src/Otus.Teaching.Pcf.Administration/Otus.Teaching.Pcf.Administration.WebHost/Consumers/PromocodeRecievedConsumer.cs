﻿using MassTransit;
using Messages;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class PromocodeRecievedConsumer : IConsumer<PromocodeRecieved>
    {
        private readonly IEmployeeService _employeeService;

        public PromocodeRecievedConsumer(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public async Task Consume(ConsumeContext<PromocodeRecieved> context)
        {
            if (context.Message.PartnerManagerId.HasValue)
                await _employeeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId.Value);
        }
    }
}
